//
//  AppDelegate.swift
//  BibkyMetaGenerator
//
//  Created by Anirban Maiti on 6/14/17.
//  Copyright © 2017 com.bibky. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

