//
//  BibkyAnalytics.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 4/27/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit
import Firebase
import FirebaseCrash

class BibkyAnalytics: NSObject {
    // Can't init is singleton
    private override init() {}
    private var startPlayTime:Date? = nil
    
    static let shared: BibkyAnalytics = BibkyAnalytics()
    
    func trackEvent(_ category: String, action: String, label: String, value: NSNumber?) {
        let builder = GAIDictionaryBuilder.createEvent(withCategory: category, action: action, label: label, value: value)
        GAI.sharedInstance().defaultTracker.send(builder?.build() as [NSObject : AnyObject]!)
    }

    func trackBeginPlay(storyName: String) {
        FirebaseCrashMessage("trackStartPlay: " + storyName)
        trackEvent("Story", action: "beginPlay", label: storyName, value: nil)
        startPlayTime = Date()
    }

    func trackResumePlay(storyName: String) {
        FirebaseCrashMessage("trackStartPlay: " + storyName)
        trackEvent("Story", action: "resumePlay", label: storyName, value: nil)
        startPlayTime = Date()
    }

    func trackEndPlay(storyName: String) {
        FirebaseCrashMessage("trackFinishedPlay: " + storyName)
        trackEvent("Story", action: "finishedPlay", label: storyName, value: nil)
        self.trackPlaySessionPercentage(storyName: storyName)
    }
    
    func trackPausePlay(storyName: String) {
        FirebaseCrashMessage("trackPausePlay: " + storyName)
        trackEvent("Story", action: "pausePlay", label: storyName, value: nil)
        self.trackPlaySessionPercentage(storyName: storyName)
    }
    
    private func trackPlaySessionPercentage(storyName: String) {
        // calculate the play time from the begin or resume play
        
        if (startPlayTime == nil) {
            return
        }
        let now = Date()
        let diff:Int = now.minutes(from: startPlayTime!)
        startPlayTime = nil
        let percentage:NSNumber = NSNumber(value: diff)
        FirebaseCrashMessage("trackPlayPercentage: " + storyName)
        trackEvent("PlaySession", action: "percentage", label: storyName, value: percentage)
    }

    
    func logMsg(_ msg: String) {
        print(msg)
        FirebaseCrashMessage(msg)
    }

}

