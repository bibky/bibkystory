//
//  StepCountAction.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 3/28/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit
import CoreMotion

class StepCountAction: BibkyAction {
    fileprivate let pedometerManager = CMPedometer()
    var timer: Timer? = nil
    
//    public override init(_ metadata: BibkyActionMeta, delegate: BibkyActionDelegate) {
//        super.init(metadata, delegate: delegate)
//    }
    
    override func start() {
        startUpdatingActivity()
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(StepCountAction.update), userInfo: nil, repeats: true);
        NotificationCenter.default.post(name: bibkyNotificationName, object: "Debug: Please Walk and count steps.")
        showActionInstruction()
    }
    
    override func stop() {
        if CMPedometer.isStepCountingAvailable() {
            pedometerManager.stopUpdates()
        }
        self.timer?.invalidate()
    }
    
    func startUpdatingActivity() {
//        if CMMotionActivityManager.isActivityAvailable() {
//            self.pedometerManager.startEventUpdates(handler: { (event: CMPedometerEvent?, error:Error?) in
//                <#code#>
//            })(to: OperationQueue.main, withHandler: {
//                [weak self] (data: CMMotionActivity?) in
//                DispatchQueue.main.async(execute: {
//                    if let data = data {
//                        self?.isInState = data.walking
//                        
//                        NSLog("stationary: \(data.stationary)")
//                        NSLog("walking: \(data.walking)")
//                        NSLog("running: \(data.running)")
//                        NSLog("automotive: \(data.automotive)")
//                        NSLog("unknown: \(data.unknown)")
//                        NSLog("confidence(0-2): \(data.confidence.rawValue)")
//                    }
//                })
//            })
//        }
    }
    
    // must be internal or public.
    func update() {
//        if (self.isInState) {
//            self.actionCounter += 1
//            NotificationCenter.default.post(name: bibkyNotificationName, object: "Walking detected. Count: \(self.actionCounter)")
//            if self.actionCounter >= metadata.actionCount {
//                self.onFinishAction()
//            }
//        }
    }

}
