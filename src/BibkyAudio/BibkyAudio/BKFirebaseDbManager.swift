//
//  BKFirebaseDbManager.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 4/28/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit
import Firebase

class BKFirebaseDbManager: NSObject {

    var ref: DatabaseReference!
    var bibkyStories = [StoryInfo]()
    var loaded: Bool = false
    var observerEnabled: Bool = false
    
    // Can't init is singleton
    private override init() {
    }
    
    static let shared: BKFirebaseDbManager = BKFirebaseDbManager()
    
    func configure() {
        ref = Database.database().reference()
        subscribe()
    }
    
    private func getStories(with block: @escaping ([StoryInfo]) -> Swift.Void) {
        if loaded {
            block(bibkyStories)
        } else {
            if (!observerEnabled) {
            readStories(with: block)
            }
        }
    }
    
    func subscribe() {
        if (!observerEnabled) {
            let storiesRef = ref.child("Stories")
            observerEnabled = true
            storiesRef.observe(.value, with: { snapshot in
                
                var bibkyStories = [StoryInfo]()
                
                if !snapshot.exists() { return }
                
                if snapshot.hasChildren() {
                    for rest in snapshot.children.allObjects as! [DataSnapshot] {
                        guard let restDict = rest.value as? [String: AnyObject] else {
                            continue
                        }
                        print(restDict)
                        if let id = restDict["id"] as? Int,
                            let location = restDict["location"] as? String,
                            let name = restDict["name"] as? String,
                            let description = restDict["description"] as? String,
                            let length = restDict["length"] as? String,
                            let authors = restDict["authors"] as? String,
                            let version = restDict["version"] as? Double
                        {
                            let s = StoryInfo()
                            s.id = id
                            s.location = location
                            s.name = name
                            s.version = version
                            s.story_desc = description
                            s.length = length
                            s.authors = authors
                            
                            bibkyStories.append(s)
                        }
                    }
                }
                BKLocalDbManager.shared.updateAllStories(bibkyStories)
            })
        }
    }
    
    private func readStories(with block: @escaping ([StoryInfo]) -> Swift.Void) {
        
        let storiesRef = ref.child("Stories")
        observerEnabled = true
        storiesRef.observe(.value, with: { snapshot in
    
            self.bibkyStories = [StoryInfo]()
            
            if !snapshot.exists() { return }

            if snapshot.hasChildren() {
                for rest in snapshot.children.allObjects as! [DataSnapshot] {
                    guard let restDict = rest.value as? [String: AnyObject] else {
                        continue
                    }
                    print(restDict)
                    if let id = restDict["id"] as? Int,
                        let location = restDict["location"] as? String,
                        let name = restDict["name"] as? String,
                        let description = restDict["description"] as? String,
                        let length = restDict["length"] as? String,
                        let authors = restDict["authors"] as? String,
                        let version = restDict["version"] as? Double
                    {
                        let s = StoryInfo()
                        s.id = id
                        s.location = location
                        s.name = name
                        s.version = version
                        s.story_desc = description
                        s.length = length
                        s.authors = authors
                        
                        self.bibkyStories.append(s)
                    }
                }
            }
            BKLocalDbManager.shared.updateAllStories(self.bibkyStories)
            self.loaded = true
            block(self.bibkyStories)

            // can also use
            // snapshot.childSnapshotForPath("full_name").value as! String
        })
    }

}
