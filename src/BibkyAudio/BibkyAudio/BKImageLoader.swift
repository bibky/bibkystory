//
//  BKImageLoader.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 6/6/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

class BKImageLoader: NSObject {
    
    var cachedImages:[String: UIImage] = [:]
    // Can't init is singleton
    private override init() {
    }
    
    static let shared: BKImageLoader = BKImageLoader()
    
    func add(key: String, image: UIImage) {
        cachedImages[key] = image
    }
    
    func get(key: String) -> UIImage?{
        return cachedImages[key]
    }
    
    func clear() {
        cachedImages = [:]
    }
    
}

extension UIImageView {
    public func renderThumbnailImage(storyLocation: String, updateImage: Bool) {
        
        DispatchQueue.global().async(execute: {
            BKDataManager.shared.getThumbnailImage(storyLocation: storyLocation, forceDownload: updateImage) { (image) in
                DispatchQueue.main.async(execute: { () -> Void in
                    self.image = image
                })
            }
        })
    }
    public func renderBackgroundImage(storyLocation: String, updateImage: Bool) {
        DispatchQueue.global().async(execute: {
            BKDataManager.shared.getBackgroundImage(storyLocation: storyLocation, forceDownload: updateImage) { (image) in
                DispatchQueue.main.async(execute: { () -> Void in
                    self.image = image
                })
            }
        })
    }
}
