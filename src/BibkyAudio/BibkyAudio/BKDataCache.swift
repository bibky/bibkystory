//
//  BKDataCache.swift
//  BibkyPlay
//
//  Created by Anirban Maiti on 6/14/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

public enum BCacheDataType : Int {
    case Images
    case StoryList
    case Metadata
}


class BKDataCache: NSObject {
    
    var cachedImages:[String: UIImage] = [:]
    var bibkyStories:[StoryInfo] = []
    var bibkyStoryMetadata:[BibkyStoryMeta] = []

    // Can't init is singleton
    private override init() {
    }
    
    static let shared: BKDataCache = BKDataCache()

    func addImage(key: String, image: UIImage) {
        cachedImages[key] = image
    }
    
    func getImage(key: String) -> UIImage?{
        return cachedImages[key]
    }
    
    func clearImages() {
        cachedImages = [:]
    }

}

extension BKDataCache {

    func addStory(_ story: StoryInfo) {
        bibkyStories.append(story)
    }
    
    func getStories() -> [StoryInfo]{
        return bibkyStories
    }
    
    func removeStory(_ id: Int) {
        var index = 0
        for story in bibkyStories {
            if story.id == id {
                bibkyStories.remove(at: index)
                break
            }
            index += 1
        }
    }
    
    func clearStories() {
        bibkyStories = []
    }
}

extension BKDataCache {
    
    func addMetadata(_ meta: BibkyStoryMeta) {
        bibkyStoryMetadata.append(meta)
    }
    
    func getMetadata() -> [BibkyStoryMeta]?{
        return bibkyStoryMetadata
    }
    
    func clearMetadata() {
        bibkyStoryMetadata = []
    }
}
