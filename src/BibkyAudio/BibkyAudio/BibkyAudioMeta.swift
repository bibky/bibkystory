//
//  BibkyAudioMeta.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 3/28/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

class BibkyAudioMeta: NSObject {

    fileprivate(set) var fileName: String
    fileprivate(set) var fileExt: String = "mp3"
    fileprivate(set) var directory: String? = nil
    var loop: Bool = false
    var loopDelay: Double = 0.0
    var vol: Double  = 1.0
    var delay: Double = 0.0
    var playTimer: Double = 0.5
    
    public init(_ path:String, loop: Bool) {
        
        var words = path.components(separatedBy: "/")
        let fileNameWords = words.removeLast().components(separatedBy: ".")
        
        
//        var range: Range<String.Index> = path.range(of: "/", options: NSString.CompareOptions.backwards, range: nil, locale: nil)!
        self.fileName = fileNameWords.first!
        self.fileExt = fileNameWords.last!
        self.directory = words.joined(separator: "/")
        self.loop = loop
    }

}
