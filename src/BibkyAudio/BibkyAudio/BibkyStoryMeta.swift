//
//  BibkyStoryMeta.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 3/28/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

public enum BibkyPlayerVersion : Int {
    case Ver1
    case Ver2
    case Ver3
}


class BibkyStoryMeta: NSObject {
    
    fileprivate(set) var version: BibkyPlayerVersion = BibkyPlayerVersion.Ver1

    fileprivate(set) var storyId: Int
    fileprivate(set) var storyTitle: String
    var storyDescription: String? = ""
    var storyAuthors: String? = ""
    var storyLength: String? = ""
    var storyLocation: String = ""
    
    fileprivate(set) var bgMusic: BibkyAudioMeta? = nil
    fileprivate(set) var chapters = [BibkyChapterMeta]()
    

    
    public init(_ info: StoryInfo) {
        self.storyTitle = info.name
        self.version = info.playerVersion
        self.storyId = info.id
    }
    
    func setBackgroundMusic(_ media: BibkyAudioMeta) {
        self.bgMusic = media
    }

    func addChapter(_ id: Int) -> BibkyChapterMeta {
        let chapter = BibkyChapterMeta(id)
        self.chapters.append(chapter)
        return chapter
    }
    
    func chapter(id: Int) -> BibkyChapterMeta? {
        for chapter in chapters {
            if chapter.Id == id {
                return chapter
            }
        }
        return nil
    }
}
