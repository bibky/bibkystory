//
//  BibkyActionContainer.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 4/27/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

class BibkyActionContainerMeta: NSObject {
    
    var actionAudio: BibkyAudioMeta? = nil
    var actionEndAudio: BibkyAudioMeta? = nil
    var instruction: String? = nil
    
    // version2
    var startTime: Double = 0.0
    var endTime: Double = 0.0
    var repeatStartTime: Double = 0.0
    var endActionJumpTime: Double = 0.0
    var repeatDelay: Double = 30 // seconds
    var disableEndActionJump: Bool = false
    
    // version 3
    var id: Int
    fileprivate(set) var actions = [BibkyActionMeta]()
    
    
    public init(_ id: Int) {
        self.id = id
    }
    
    func addAction(_ actionType: BibkyActionType) -> BibkyActionMeta {
        let action = BibkyActionMeta(actionType)
        self.actions.append(action)
        return action
    }

}
