//
//  StoryInfo.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 4/28/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

class StoryInfo: NSObject {
    var id = 0
    var playerVersion = BibkyPlayerVersion.Ver1
    var name = ""
    var location = ""
    var length = ""
    var story_desc = ""
    var version = 0.0
    var authors = ""
    var bgColor = "#FFFFFF"
    
    // processed setters
    var downloaded = false
}
