//
//  BKLocalDbManager.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 4/28/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit
import CoreData

class BKLocalDbManager: NSObject {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    // Can't init is singleton
    private override init() {
    }
    
    static let shared: BKLocalDbManager = BKLocalDbManager()
    
    func configure() {
    }
    
    func preloadData() {
        deleteStories()
        if let json:[String: Any] = readDocumentDirectoryJson(fileName: "BibkyStories", subdirectory: "Stories"),
            let stories = json["stories"] as? [[String: Any]] {
            for story in stories {
                if let id = story["id"] as? Int,
                    let location = story["location"] as? String,
                    let name = story["name"] as? String,
                    let description = story["description"] as? String,
                    let length = story["length"] as? String,
                    let authors = story["authors"] as? String,
                    let version = story["version"] as? Double
                {
                    let storyInfo = StoryInfo()
                    storyInfo.id = id
                    storyInfo.location = location
                    storyInfo.name = name
                    storyInfo.story_desc = description
                    storyInfo.length = length
                    storyInfo.authors = authors
                    storyInfo.version = version
                    storyInfo.downloaded = true
                    
                    updateDbStory(storyInfo)
                }
            }
        }
    }
    
    func deleteStories() {
        let _fetchRequest:NSFetchRequest<Story> = Story.fetchRequest()
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: _fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
        }
        BKDataCache.shared.clearStories()
        NotificationCenter.default.post(name: bkUpdateStoreListNotification, object: nil)
    }

    
    func addToUpNext(_ id: Int) {
        UserDefaults.standard.set(id, forKey: "upNextStoryId")
    }
    
    func getUpNext() -> Int {
        return (UserDefaults.standard.integer(forKey: "upNextStoryId"))
    }
    
    func resetUpNext() {
        UserDefaults.standard.removeObject(forKey: "upNextStoryId")
    }
    
    
    public func fetchStories() -> [Story]?{
        var stories: [Story] = []
        do {
            stories = try context.fetch(Story.fetchRequest())
        }
        catch {
            print("Fetching Failed")
        }
        return stories
    }
    
    private func fetchStoryById(_ storyId: Int) -> Story? {
        do {
            let _fetchRequest:NSFetchRequest<Story> = Story.fetchRequest()
            _fetchRequest.predicate = NSPredicate(format: "(id == %d)", storyId)
            let results = try context.fetch(_fetchRequest)
            if results.count > 0 {
                return results.first!
            }
        }
        catch {
            print("Fetching Failed")
        }
        return nil
    }

    
    func updateAllStories(_ infos: [StoryInfo]) {
        
        BKDataCache.shared.clearStories()
        
        for _story: StoryInfo in infos {
            self.updateDbStory(_story)
        }
        NotificationCenter.default.post(name: bkUpdateStoreListNotification, object: nil)
    }
    
    func updateDbStory (_ s: StoryInfo) {
        
        BKDataCache.shared.removeStory(s.id)
        
        if let dbStory: Story = fetchStoryById(s.id) {
            dbStory.name = s.name
            dbStory.location = s.location
            dbStory.story_desc = s.story_desc
            dbStory.length = s.length
            dbStory.authors = s.authors
            if (!dbStory.downloadState) {
                dbStory.downloadState = s.downloaded
            } else {
                s.downloaded = true
            }
            if (dbStory.version != s.version) {
                dbStory.downloadState = false
                s.downloaded = false
            }

            dbStory.version = s.version
            dbStory.bgColor = s.bgColor
//            dbStory.downloadState = s.downloaded
            // Save the data to coredata
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        } else {
            let dbStory = Story(context: context)
            dbStory.id = Int16(s.id)
            dbStory.name = s.name
            dbStory.location = s.location
            dbStory.story_desc = s.story_desc
            dbStory.length = s.length
            dbStory.authors = s.authors
            dbStory.version = s.version
            dbStory.bgColor = s.bgColor
            dbStory.pausedChapterId = -1
            dbStory.pausedMusicTime = 0.0
            dbStory.downloadState = s.downloaded
            
            // Save the data to coredata
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
        BKDataCache.shared.addStory(s)
    }
    
    func markStoryDownloaded (_ s: StoryInfo) {
        BKDataCache.shared.removeStory(s.id)
        if let dbStory: Story = fetchStoryById(s.id) {
            dbStory.downloadState = true
            s.downloaded = true
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
        BKDataCache.shared.addStory(s)
    }

    func saveStoryState (_ s: BKStoryState) {
        if let dbStory: Story = fetchStoryById(s.storyId) {
            dbStory.pausedChapterId = Int16(s.chapterState.chapterId)
            dbStory.pausedMusicTime = s.chapterState.musicTime
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
    }

    func readStoryState (storyId:Int) -> BKStoryState {
        
        do {
            let _fetchRequest:NSFetchRequest<Story> = Story.fetchRequest()
            _fetchRequest.predicate = NSPredicate(format: "(id == %d)", storyId)
            let results = try context.fetch(_fetchRequest)
            if results.count > 0 {
                return BKStoryState(storyId: storyId, chapterState: BKChapterState(chapterId: Int((results.first?.pausedChapterId)!), musicTime: (results.first?.pausedMusicTime)!))
            }
        }
        catch {
            print("Fetching Failed")
        }
        return BKStoryState()
    }

    
    func deleteDbStory(_ s: StoryInfo) {
        if let dbStory: Story = fetchStoryById(s.id) {
            context.delete(dbStory)
            // Save the data to coredata
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
    }
    
    
    private func readDocumentDirectoryJson(fileName: String, subdirectory: String) -> [String: Any]? {
        do {
            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            let documentsDirectory = paths[0]
            let file = documentsDirectory.appendingPathComponent(subdirectory).appendingPathComponent("\(fileName).json")
            
            if FileManager.default.fileExists(atPath: file.path) {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    return object
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return [:]
    }
    
}
