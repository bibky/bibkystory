//
//  BibkyStoryPlayer.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 3/28/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

struct BKStoryState {
    var storyId:Int = -1
    var chapterState:BKChapterState = BKChapterState()
}

class BKStoryPlayer: NSObject {

    fileprivate(set) var metadata: BibkyStoryMeta
    var currentChapterPlayer: BKChapterPlayer? = nil
    
    
    public init(_ metadata: BibkyStoryMeta) {
        self.metadata = metadata
    }

    
    func start(_ state:BKStoryState = BKStoryState()) {
        NotificationCenter.default.post(name: bibkyNotificationInstruction, object: "")
        self.playChapter(state)
    }
    
    func stop() {
        self.stopChapter()
    }
    
    func playChapter(_ state:BKStoryState = BKStoryState()) {
        var chapterMeta:BibkyChapterMeta
        if(state.chapterState.chapterId == -1) {
            chapterMeta = metadata.chapters[0]
        } else {
        chapterMeta = metadata.chapter(id: state.chapterState.chapterId)!
        }
        BibkyAnalytics.shared.logMsg("Starting chapter: " + String(chapterMeta.Id))
        currentChapterPlayer?.stop()
        currentChapterPlayer = BKChapterPlayer(chapterMeta, storyPlayer: self)
        self.currentChapterPlayer?.play(state)
    }

    func stopChapter() {
        NotificationCenter.default.post(name: bibkyNotificationInstruction, object: "")
        currentChapterPlayer?.stop()
    }
    
    func getCurrentState() -> BKStoryState {
        return BKStoryState(storyId: metadata.storyId, chapterState: (currentChapterPlayer?.getChapterState())!)
    }
    
}
