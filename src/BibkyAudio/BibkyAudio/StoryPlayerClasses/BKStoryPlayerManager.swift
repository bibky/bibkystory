//
//  BKStoryPlayerManager.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 4/30/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

class BKStoryPlayerManager: NSObject {
    
    // Can't init is singleton
    private override init() {
    }
    
    static let shared: BKStoryPlayerManager = BKStoryPlayerManager()
    
    var bibkyStoryPlayer: BKStoryPlayer? = nil
    var _storyStarted: Bool = false
    var currentPlayingStoryMeta: BibkyStoryMeta? = nil
    
    func configure() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onBibkyShakeNotification), name: shakeNotificationName, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onBibkyPauseNotification), name: bkPausePlayingNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onBibkyFinishNotification), name: bkFinishedPlayingNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onStartoverStoryNotification), name: bkStartoverStoryNotification, object: nil)
        
    }
    
    func onBibkyShakeNotification(notification : NSNotification) {
        print("RECEIVED SPECIFIC NOTIFICATION: \(notification)")
        // if story is not sharted start the play
        if !_storyStarted,
            let storyId: Int = BKLocalDbManager.shared.getUpNext(),
            storyId != 0 {
            
            _storyStarted = true
            
            // build the selected story and start
            currentPlayingStoryMeta = BKDataManager.shared.getStoryMetadata(byId: storyId)
            self.bibkyStoryPlayer = BKStoryPlayer(currentPlayingStoryMeta!)
            
            let savedState:BKStoryState = self.readSavedState(storyId)
            if (savedState.chapterState.chapterId == -1) {
                BibkyAnalytics.shared.trackBeginPlay(storyName: (currentPlayingStoryMeta?.storyTitle)!)
            } else {
                BibkyAnalytics.shared.trackResumePlay(storyName: (currentPlayingStoryMeta?.storyTitle)!)
            }
            self.bibkyStoryPlayer?.start(savedState)
            NotificationCenter.default.post(name: bkStartedPlayingNotification, object: storyId)
            Helper.changeDeviceFeaturesOnStartStory()

        }
    }
    
    func onBibkyFinishNotification(notification : NSNotification) {
        print("RECEIVED SPECIFIC NOTIFICATION: \(notification)")
        BibkyAnalytics.shared.trackEndPlay(storyName: (currentPlayingStoryMeta?.storyTitle)!)
        self.bibkyStoryPlayer?.stop()
        self.resetSavedState()
        _storyStarted = false
        currentPlayingStoryMeta = nil
        Helper.changeDeviceFeaturesOnStopStory()
    }
    
    func onStartoverStoryNotification(notification : NSNotification) {
        print("RECEIVED SPECIFIC NOTIFICATION: \(notification)")
        BibkyAnalytics.shared.trackPausePlay(storyName: (currentPlayingStoryMeta?.storyTitle)!)
        self.bibkyStoryPlayer?.stop()
        self.resetSavedState()
        _storyStarted = false
        currentPlayingStoryMeta = nil
        Helper.changeDeviceFeaturesOnStopStory()
    }
    
    
    func onBibkyPauseNotification(notification : NSNotification) {
        print("RECEIVED SPECIFIC NOTIFICATION: \(notification)")
        // TO-DO: change this to save story state
        if (currentPlayingStoryMeta  == nil) {
            return
        }
        
        Helper.changeDeviceFeaturesOnStopStory()
        BibkyAnalytics.shared.trackPausePlay(storyName: (currentPlayingStoryMeta?.storyTitle)!)
        
        // save the story state
        let storyState:BKStoryState = (self.bibkyStoryPlayer?.getCurrentState())!
        self.saveState(storyState)
        
        self.bibkyStoryPlayer?.stop()
        _storyStarted = false
        currentPlayingStoryMeta = nil
    }
    
    func saveState(_ state: BKStoryState) {
        BKLocalDbManager.shared.saveStoryState(state)
    }
    
    func resetSavedState() {
        BKLocalDbManager.shared.saveStoryState(BKStoryState(storyId: (currentPlayingStoryMeta?.storyId)!, chapterState: BKChapterState()))
    }
    
    
    func readSavedState(_ storyId:Int) -> BKStoryState{
        return BKLocalDbManager.shared.readStoryState(storyId: storyId)
    }
    
    
}
