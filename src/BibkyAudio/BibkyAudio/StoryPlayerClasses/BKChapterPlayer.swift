//
//  BKChapterPlayer.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 5/3/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

struct BKChapterState {
    var chapterId:Int = -1
    var musicTime:Double = 0.0
}


class BKChapterPlayer: NSObject {
    
    fileprivate(set) var metadata: BibkyChapterMeta
    unowned var storyPlayer: BKStoryPlayer
    
    var chapterBgPlayer: BKMusicPlayer? = nil
    
    let dispatchQueue = DispatchQueue(label: "com.bibky.chapterplayer.DispatchQueue", attributes:DispatchQueue.Attributes.concurrent)
    var actionContainerPlayer:BKActionContainerPlayer? = nil
    
    public init(_ metadata: BibkyChapterMeta, storyPlayer: BKStoryPlayer) {
        self.metadata = metadata
        self.storyPlayer = storyPlayer
        
        if(metadata.backgroundAudio != nil) {
            self.chapterBgPlayer = BKMusicPlayer(metadata.backgroundAudio)
        }
    }
    
    public func play(_ savedState:BKStoryState = BKStoryState()) {
        
        dispatchQueue.async {
            // Find the action container index based on the saved music time
            var index = 0
            var calculatedStartTime = 0.0
            if (savedState.chapterState.musicTime != 0.0) {
                for action_container in self.metadata.actionContainers {
                    if action_container.endTime >= savedState.chapterState.musicTime {
                        break
                    } else {
                        index += 1
                    }
                }
            }
            
            // Logic being we resume from the end of the immediate previous action 
            if (index > 0) {
                calculatedStartTime = self.metadata.actionContainers[index - 1].endTime
            }

            // start the music player with background music
            if (self.chapterBgPlayer?.play(startTime: calculatedStartTime, endBlock: { (successfullyFinished:Bool) in
                if (successfullyFinished) {
                    print("Anirban: Music successfully finished playing. End of the story")
                    NotificationCenter.default.post(name: bkFinishedPlayingNotification, object: nil)
                }
            }) == true) {
                // loop through the action container
                let semaphore = DispatchSemaphore(value: 1)
                var breakActionContainerLoop = false
                for i: Int in index ..< self.metadata.actionContainers.count  {
                    // for action_container in self.metadata.actionContainers {
                    // Action container Player would do -
                    // 1. collect the associated actions.
                    // 2. add time observer to player and recieve events whenever the time is reached.
                    // 3. When the time is reached, start the action
                    // 4. Handle action complete events and route it to next plan.
                    // 5. ie, callback the handler right here with the event metadata
                    
                    // 6. Here we decide, if we have to play another chapter or go to the next action container
                    // 7. If we need to play another chapter, tell the story player to start next chapter with Id
                    
                    semaphore.wait(timeout: DispatchTime.distantFuture)
                    if(breakActionContainerLoop) {
                        semaphore.signal()
                        break
                    }
                    let action_container = self.metadata.actionContainers[i]
                    self.actionContainerPlayer = BKActionContainerPlayer(action_container, player: self.chapterBgPlayer!)
                    self.actionContainerPlayer?.start(finishDelegate: { (status:BKActionContainerStatus) in
                        if(status.nextChapterId != 0) {
                            self.stop()
                            let state:BKStoryState = BKStoryState(storyId: -1, chapterState: BKChapterState(chapterId: status.nextChapterId, musicTime: 0.0))
                            self.storyPlayer.playChapter(state)
                            breakActionContainerLoop = true
                        } else if(status.isInactive) {
                            NotificationCenter.default.post(name: bkPausePlayingNotification, object: nil)
                            breakActionContainerLoop = true
                        }
                        semaphore.signal()
                    })
                }
                
            } else {
                // Failed to start playing, so end of story
                print("Anirban: Music successfully finished playing. End of the story")
                NotificationCenter.default.post(name: bkFinishedPlayingNotification, object: nil)
            }
            
        }
    }
    
    func stop() {
        self.chapterBgPlayer?.stop(1.0)
        self.actionContainerPlayer?.cleanUp()
    }
    
    func getChapterState() -> BKChapterState {
        return BKChapterState(chapterId: self.metadata.Id, musicTime: (self.chapterBgPlayer?.currentTime())!)
    }
    
}
