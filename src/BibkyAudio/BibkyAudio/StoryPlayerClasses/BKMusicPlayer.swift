//
//  BKMusicPlayer.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 5/5/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit
import AVFoundation

class BKMusicPlayer: NSObject, AVAudioPlayerDelegate {
    
    var musicPlayer:AVAudioPlayer? = nil
    var observerTimer: Timer? = nil
    var playTimer:Double = 0.5
    var observeEvents:[Double:String] = [:]
    let lockQueue = DispatchQueue(label: "com.bibky.observer.LockQueue", attributes:DispatchQueue.Attributes.concurrent)
    let timerQueue = DispatchQueue(label: "com.bibky.observer.TimerQueue", attributes:DispatchQueue.Attributes.concurrent)
    
    fileprivate(set) var onEndBlock: ((Bool)->Void)?
    
    public init (_ media: BibkyAudioMeta?) {
        
        if media == nil {
            return
        }
        
        do {
            playTimer = (media?.playTimer)!
            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            let documentsDirectory = paths[0]
            let fileNameWithExt = media?.fileName.appendingFormat(".%@", (media?.fileExt)!)
            //                let fileExt = self.mediaMetadata?.fileExt
            let url: URL = documentsDirectory.appendingPathComponent((media?.directory)!).appendingPathComponent(fileNameWithExt!)
            
            if FileManager.default.fileExists(atPath: url.path) {
                
                musicPlayer = try AVAudioPlayer(contentsOf: url)
                if ((media?.loop)! && media?.loopDelay == 0.0) {
                    musicPlayer?.numberOfLoops = -1
                } else {
                    musicPlayer?.numberOfLoops = 0
                }
                musicPlayer?.volume = Float((media?.vol)!)
                musicPlayer?.prepareToPlay()
            } else {
                NotificationCenter.default.post(name: bibkyNotificationName, object: "Debug: No audio file found \(url.path).")
            }
        } catch let error {
            print(error)
            musicPlayer = nil
        }
    }
    
    func play(startTime:Double = 0.0, endBlock block: @escaping ((Bool)->Void)) -> Bool{
        if (musicPlayer == nil) {
            return false
        }
        self.onEndBlock = block
        musicPlayer?.delegate = self
        if (startTime != 0.0) {
            musicPlayer?.currentTime = startTime
        }
        musicPlayer?.play()
        return true
    }
    
    func resume() {
        musicPlayer?.play()
    }
    
    func seek(time: Double) {
//        musicPlayer?.pause()
        musicPlayer?.currentTime = time
        musicPlayer?.prepareToPlay()
        musicPlayer?.setVolume(1, fadeDuration: 0.0)
    }
    
    public func currentTime() -> Double? {
        return musicPlayer?.currentTime
    }
    
    
    func changeVolume(vol: Double, fadeDuration: Double=0.0) {
        musicPlayer?.setVolume(Float(vol), fadeDuration: fadeDuration)
    }
    
    func stop(_ delay:Double=0.0) {
        if (musicPlayer != nil) {
            musicPlayer?.setVolume(0, fadeDuration: delay)
            DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                self.musicPlayer?.stop()
                self.musicPlayer = nil
            }
        }
    }
    func pause() {
        if (musicPlayer != nil && (musicPlayer?.isPlaying)!) {
            //            musicPlayer.setVolume(0, fadeDuration: 1.0)
            self.musicPlayer?.pause()
        }
    }
    
    public func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.onEndBlock!(flag)
    }
}

extension BKMusicPlayer {
    
    public func startObserver(events:[Double:String], with block:@escaping ((String)->Void)) {
        
        lockQueue.sync {
            self.observeEvents = events
        }
        
        var sortedEventKeys:[Double] = []
        stopObserver()
        self.observerTimer = Timer.scheduledTimer(withTimeInterval: self.playTimer, repeats: true) { (timer) in
            self.lockQueue.sync {
                print ("\(String(describing: self.musicPlayer?.currentTime))")
                sortedEventKeys = self.observeEvents.keys.sorted()
                
                // loop through the events and find the matching one
                var index = 0
                for eventKey:Double in sortedEventKeys {
                    if (self.musicPlayer?.currentTime)! >= eventKey  {
                        block(self.observeEvents[eventKey]!)
                        self.observeEvents.removeValue(forKey: eventKey)
                        break;
                    }
                    index += 1
                }
            }
            
        }
        RunLoop.current.add(self.observerTimer!, forMode: RunLoopMode.commonModes)
    }
    public func addEventsToObserver(events:[Double:String]) {
        DispatchQueue.main.async{
            self.lockQueue.sync {
                print("added events to observer")
                self.observeEvents = events
            }
        }
    }
    
    public func stopObserver() {
        print("Music observer stopped")
        if(observerTimer != nil) {
            observerTimer?.invalidate()
            observerTimer = nil
        }
    }
}
