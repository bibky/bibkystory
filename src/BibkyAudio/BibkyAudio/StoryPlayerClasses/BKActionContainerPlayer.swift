//
//  BKActionContainerPlayer.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 5/5/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

struct BKActionContainerStatus {
    var nextChapterId:Int = 0
    var isInactive:Bool = false
    var actionFinished = false
}

class BKActionContainerPlayer: NSObject {
    
    fileprivate(set) var metadata: BibkyActionContainerMeta
    fileprivate(set) var musicPlayer: BKMusicPlayer
    fileprivate(set) var finishDelegate: ((BKActionContainerStatus)->Void)?
    let dispatchQueue:DispatchQueue = DispatchQueue(label: "com.bibky.actioncontainerplayer.DispatchQueue", attributes:DispatchQueue.Attributes.concurrent)
    var activeActions:[BibkyAction] = []
    var worker:DispatchWorkItem? = nil
    var noActionTimer:Timer? = nil
    
    public init(_ metadata: BibkyActionContainerMeta, player:BKMusicPlayer) {
        self.metadata = metadata
        self.musicPlayer = player
    }
    
    func start(finishDelegate: @escaping ((BKActionContainerStatus)->Void)) {
        self.finishDelegate = finishDelegate
        // Start after a delay. We might have enough time before we start the action
        worker = getActionWorkItem()
        DispatchQueue.main.asyncAfter(deadline: .now() + getInitialTimerDelay(), execute: self.worker!)
    }
    
    func cleanUp() {
        stopActions()
        self.musicPlayer.stopObserver()
        worker?.cancel()
    }
    
    
    func getActionWorkItem() -> DispatchWorkItem {
        return DispatchWorkItem {
        // 1. Start the observer with list of events. Observer should callback with one event that is met. eg, [{"event_name1", event_time1}, {"event_name2", event_time2}]
        // 2. Begin event should start the event,
        // 3. End event would start the restart process after a delay if specified
        let events:[Double:String] = [self.metadata.startTime: "start", self.metadata.endTime: "end"]
        
        var repeatActionPlayerTimer:Timer? = nil
        self.musicPlayer.startObserver(events: events, with: { (eventName) in
            if (eventName == "start") {
                // start the actions
                self.startActions(completeBlock: { (action:BibkyActionMeta) in
                    // completeBlock is Invoked when an action takes place
                    // 1. Check if the action metadata has next chapter id
                    // else keep playing, or seek to end of the action
                    print("action performed")
                    DispatchQueue.main.async{
                        repeatActionPlayerTimer?.invalidate()
                    }
                    self.musicPlayer.stopObserver()
                    
                    if !self.metadata.disableEndActionJump {
                        let seekTo = self.metadata.endActionJumpTime != 0.0 ? self.metadata.endActionJumpTime : self.metadata.endTime
                        self.musicPlayer.seek(time: seekTo)
                    }
                    self.musicPlayer.resume()
                    
                    self.finishDelegate!(BKActionContainerStatus(nextChapterId: action.nextChapterId, isInactive: false, actionFinished: true))
                }, noActionBlock: {
                    print("no action within 120s")
                    // delegate this to storyPlayer to save and quit
                    let status = BKActionContainerStatus(nextChapterId: 0, isInactive: true, actionFinished: false)
                    self.finishDelegate!(status)
                    DispatchQueue.main.async{
                        repeatActionPlayerTimer?.invalidate()
                    }
                })
            }
            if (eventName == "end") {
                // clear the Observers so that we can add new once
                self.musicPlayer.addEventsToObserver(events: [:])
                
                // Pause the audio and repeat the audio after some time
                DispatchQueue.main.async{
                    if (repeatActionPlayerTimer  == nil || !(repeatActionPlayerTimer?.isValid)!) {
                        repeatActionPlayerTimer = Timer.scheduledTimer(withTimeInterval: self.metadata.repeatDelay, repeats: false) {
                            (_) in
                            print("start over")
                            self.musicPlayer.pause()
                            self.musicPlayer.seek(time: self.metadata.repeatStartTime)
                            self.musicPlayer.addEventsToObserver(events: [self.metadata.endTime: "end"])
                            self.musicPlayer.resume()
                        }
                    }
                }
                
                if (!self.metadata.disableEndActionJump) {
                    print("loop started")
                    self.musicPlayer.seek(time: self.metadata.startTime)
                    self.musicPlayer.addEventsToObserver(events: [self.metadata.endTime: "end"])
                    self.musicPlayer.resume()
                } else {
                    self.musicPlayer.pause()
                }
                
            }
            
//            if (eventName == "end") {
//                
//            }
        })
                }
    }
    
    func startActions(completeBlock block:@escaping ((BibkyActionMeta)->Void), noActionBlock _noActionBlock:@escaping (()->Void)) {
        
        // start the no action check timer
        
        DispatchQueue.main.async{
            self.noActionTimer = Timer.scheduledTimer(withTimeInterval: 120, repeats: false) {
                (_) in
                _noActionBlock()
            }
        }
        
        
        for action in metadata.actions {
            let performAction = BibkyActionFactory.createAction(action, completionHandler: { (actionMeta: BibkyActionMeta) in
                
                BibkyAnalytics.shared.logMsg("Action performed: " + String(describing: action.type))
                DispatchQueue.main.async {
                    self.noActionTimer?.invalidate()
                }
                self.stopActions()
                block(actionMeta)
            })
            self.activeActions.append(performAction!)
            performAction?.start()
        }
    }
    
    func stopActions() {
        for action in self.activeActions {
            action.stop()
        }
        self.noActionTimer?.invalidate()
        self.activeActions.removeAll()
    }
    
    
}

// Helper methods
extension BKActionContainerPlayer {
    
    func getInitialTimerDelay() -> Double {
        let currentTime:Double = musicPlayer.currentTime()!
        let actionStartTime = metadata.startTime
        if actionStartTime - currentTime > 5 {
            return actionStartTime - currentTime - 5
        }
        return 0
    }
}
