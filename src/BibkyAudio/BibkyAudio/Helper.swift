//
//  Helper.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 4/28/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

func convertStringTime(_ str: String) -> Double {
    let words = str.components(separatedBy: ":")
    
    if words.count == 2 {
        let minute = Int(words.first!)
        let second = Int(words.last!)
        let inSeconds = minute! * 60 + second!
        return Double(inSeconds)
    }
    let minute = Double(words.first!)
    let second = Double(words[1])
    let milli = Double(words.last!)
    let time = minute! * 60.0 + second! + (milli! * 0.001)
    return Double(time)
}


class Helper: NSObject {

    static var originalBrightness:CGFloat = 0.0
    
    class func storyLengthFromText(_ text: String) -> String {
        var words = text.components(separatedBy: ":")
        var result = ""
        if Int(words.first!)! != 0 {
            result = result.appendingFormat("%dh", Int(words.first!)!)
        }
        if Int(words[1])! != 0 {
            result = result.appendingFormat(" %d mimutes", Int(words[1])!)
        }
        if Int(words[2])! != 0 {
            result = result.appendingFormat(" %ds", Int(words[2])!)
        }
        return result
    }

    class func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    class func changeDeviceFeaturesOnStartStory() {
        UIScreen.main.wantsSoftwareDimming = true
        Helper.originalBrightness = UIScreen.main.brightness
        UIScreen.main.brightness = CGFloat(0.2)
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    class func changeDeviceFeaturesOnStopStory() {
        if (originalBrightness != 0.0) {
            UIScreen.main.wantsSoftwareDimming = false
            UIScreen.main.brightness = Helper.originalBrightness
            UIApplication.shared.isIdleTimerDisabled = false
        }
    }

}


extension UIImage {
    func areaAverage() -> UIColor {
        
        var bitmap = [UInt8](repeating: 0, count: 4)
        
        let context = CIContext(options: nil)
        let cgImg = context.createCGImage(CoreImage.CIImage(cgImage: self.cgImage!), from: CoreImage.CIImage(cgImage: self.cgImage!).extent)
        
        let inputImage = CIImage(cgImage: cgImg!)
        let extent = inputImage.extent
        let inputExtent = CIVector(x: extent.origin.x, y: extent.origin.y, z: extent.size.width, w: extent.size.height)
        let filter = CIFilter(name: "CIAreaAverage", withInputParameters: [kCIInputImageKey: inputImage, kCIInputExtentKey: inputExtent])!
        let outputImage = filter.outputImage!
        let outputExtent = outputImage.extent
        assert(outputExtent.size.width == 1 && outputExtent.size.height == 1)
        
        // Render to bitmap.
        context.render(outputImage, toBitmap: &bitmap, rowBytes: 4, bounds: CGRect(x: 0, y: 0, width: 1, height: 1), format: kCIFormatRGBA8, colorSpace: CGColorSpaceCreateDeviceRGB())
        
        // Compute result.
        let result = UIColor(red: CGFloat(bitmap[0]) / 255.0, green: CGFloat(bitmap[1]) / 255.0, blue: CGFloat(bitmap[2]) / 255.0, alpha: CGFloat(bitmap[3]) / 255.0)
        return result
    }
}

extension UIColor {
    
    func lighter(by percentage:CGFloat=30.0) -> UIColor? {
        return self.adjust(by: abs(percentage) )
    }
    func adjust(by percentage:CGFloat=30.0) -> UIColor? {
        var r:CGFloat=0, g:CGFloat=0, b:CGFloat=0, a:CGFloat=0;
        if(self.getRed(&r, green: &g, blue: &b, alpha: &a)){
            let r1 = round(min(r + percentage/100, 1.0) * 255)
            let g1 = round(min(g + percentage/100, 1.0) * 255)
            let b1 = round(min(b + percentage/100, 1.0) * 255)
            let hex = NSString(format: "#%02X%02X%02X", Int(r1), Int(g1), Int(b1)) as String
            print(hex)
            return UIColor(red: min(r + percentage/100, 1.0),
                           green: min(g + percentage/100, 1.0),
                           blue: min(b + percentage/100, 1.0),
                           alpha: a)
        }else{
            return nil
        }
    }
}

extension CALayer {
    
    var borderIBColor: UIColor? {
        get {
            return UIColor(cgColor: self.borderColor!)
        }
        set(newValue) {
            self.borderColor = newValue?.cgColor
        }
    }
}

extension String {
    
    func fileName() -> String {
        
        if let fileNameWithoutExtension = NSURL(fileURLWithPath: self).deletingPathExtension?.lastPathComponent {
            return fileNameWithoutExtension
        } else {
            return ""
        }
    }
    
    func fileExtension() -> String {
        
        if let fileExtension = NSURL(fileURLWithPath: self).pathExtension {
            return fileExtension
        } else {
            return ""
        }
    }
}

extension Date {
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
}
