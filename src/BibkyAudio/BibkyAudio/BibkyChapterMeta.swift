//
//  BibkyChapterMeta.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 3/28/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

class BibkyChapterMeta: NSObject {
    fileprivate(set) var Id: Int
    fileprivate(set) var narrationAudio: BibkyAudioMeta? = nil
    var backgroundAudio: BibkyAudioMeta? = nil
    var actionChime: BibkyAudioMeta? = BibkyAudioMeta("Stories/magic-chime-01.mp3", loop: false)
    fileprivate(set) var actions = [BibkyActionMeta]()
    fileprivate(set) var actions2 = [BibkyActionMeta]()
    fileprivate(set) var actionContainers = [BibkyActionContainerMeta]()

    public init(_ chapterId: Int) {
        self.Id = chapterId
    }
    
    func setNarration(_ media: BibkyAudioMeta) {
        self.narrationAudio = media
    }

    func addAction(_ actionType: BibkyActionType) -> BibkyActionMeta {
        let action = BibkyActionMeta(actionType)
        self.actions.append(action)
        return action
    }
    
    func addAction2(_ actionType: BibkyActionType, startTime: Double, endTime: Double) -> BibkyActionMeta {
        let action = BibkyActionMeta(actionType)
        action.startTime = startTime
        action.endTime = endTime
        self.actions2.append(action)
        return action
    }
    
    func addActionContainer(id: Int) -> BibkyActionContainerMeta {
        let container = BibkyActionContainerMeta(id)
        self.actionContainers.append(container)
        return container
    }

}
