//
//  NotificationDefines.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 4/30/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

let shakeNotificationName = Notification.Name("shakeNotification")
let bibkyNotificationName = Notification.Name("bibkyNotification")
let bibkyNotificationEvents = Notification.Name("bibkyNotificationEvents")
let bibkyNotificationInstruction = Notification.Name("bibkyNotificationInstruction")

let bkDownloadCompleteNotification = Notification.Name("com.bibky.downloadcomplete")


let bkRemoveFromPlaylistNotification = Notification.Name("bkRemoveFromPlaylistNotification")
let bkAddToPlaylistNotification = Notification.Name("bkAddToPlaylistNotification")
let bkNowPlayingViewAddedNotification = Notification.Name("bkNowPlayingViewAddedNotification")

let bkUpdateStoreListNotification = Notification.Name("bkUpdateStoreListNotification")

let bkStartedPlayingNotification = Notification.Name("bkStartedPlayingNotification")
let bkPausePlayingNotification = Notification.Name("bkPausePlayingNotification")
let bkFinishedPlayingNotification = Notification.Name("bkFinishedPlayingNotification")
let bkStartoverStoryNotification = Notification.Name("bkStartoverStoryNotification")

