//
//  StoryTableViewCell.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 6/5/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

class BKStoryTableViewCell: UITableViewCell {

    @IBOutlet weak var storyImage: UIImageView!
    
    
    @IBOutlet weak var storyTitle: UILabel!
    
    @IBOutlet weak var authorLabel: UILabel!
    
    @IBOutlet weak var durtionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layoutMargins = UIEdgeInsets.zero
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
