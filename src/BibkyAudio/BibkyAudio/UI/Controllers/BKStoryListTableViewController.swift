//
//  BKStoryListTableViewController.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 6/2/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit


class BKStoryListTableViewController: UITableViewController {
    
    var stories: [StoryInfo] = []
    var refreshed:Bool = false
    
    let bkgColor = UIColor(red: 241.0/255.0, green: 241.0/255.0, blue: 241.0/255.0, alpha:1.0)
    let color=UIColor(red: 200.0/255.0, green: 199.0/255.0, blue: 204.0/255.0, alpha:1.0)

    var surveyMonkeyFeedbackController:SMFeedbackViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onUpdateStoreListNotification), name: bkUpdateStoreListNotification, object: nil)

        
//        #if (arch(i386) || arch(x86_64)) && os(iOS)
        self.stories = BKDataManager.shared.storyInfoList()
//        #else
//            // Do any additional setup after loading the view.
//            BKFirebaseDbManager.shared.getStories { (storyInfos) in
//                // save stories if not present
//                self.stories = BKDataManager.shared.storyInfoList(true)
//                self.tableView.reloadData()
//            }
//        #endif
        

        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        //        refreshControl = UIRefreshControl()
        //        refreshControl?.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl?.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        tableView.sendSubview(toBack: refreshControl!)
        
        
        // Reset UpNext in case users comes back from the details view
        BKLocalDbManager.shared.resetUpNext()
        
        surveyMonkeyFeedbackController = SMFeedbackViewController.init(survey: SURVEY_MONKEY_HASH)
        surveyMonkeyFeedbackController?.delegate = self
    }
    
    func onUpdateStoreListNotification(notification : NSNotification) {
        print("RECEIVED SPECIFIC NOTIFICATION: \(notification)")
        DispatchQueue.main.async {
            self.stories = BKDataManager.shared.storyInfoList()
            self.tableView.reloadData()
        }
    }
    
    
    func refresh(sender:AnyObject) {
        // Code to refresh table view
        self.stories = BKDataManager.shared.storyInfoList(true)
        self.refreshed = true

//        BKFirebaseDbManager.shared.getStories { (storyInfos) in
//            // save stories if not present
//            self.stories = BKDataManager.shared.storyInfoList(true)
//            self.refreshed = true
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.stories.count == 0 ? 0: (self.stories.count + 1)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row >= self.stories.count {
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "lastCell")!
            return cell
        }
        
        let cell:BKStoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "storyCell")! as! BKStoryTableViewCell
        let s: StoryInfo = self.stories[indexPath.row]
        
        //        cell.storyInfo = s
        
//        if(indexPath.row % 2 == 1) {
//            cell.backgroundColor = bkgColor
//        }
        
//        cell.storyImage.renderThumbnailImage(storyLocation: s.location, updateImage: !s.downloaded)
        cell.storyImage.renderThumbnailImage(storyLocation: s.location, updateImage: false)
        cell.storyImage!.contentMode = UIViewContentMode.scaleAspectFit
        
        cell.storyTitle.text = s.name
        let len = Helper.storyLengthFromText(s.length)
        cell.durtionLabel.text = "\(len)"
        cell.authorLabel.text = "by: \(s.authors)"
        
        let cellBorder:CALayer = CALayer()
        cellBorder.borderColor = color.cgColor
        cellBorder.borderWidth = CGFloat(1.0)
        cellBorder.frame = CGRect(x: 0, y: 0, width:  tableView.frame.size.width, height: CGFloat(1.0))
        cell.layer.addSublayer(cellBorder)
        cell.layer.masksToBounds = true
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row >= self.stories.count {
            // display surver
            self.displaySurvey()
        }
    }
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //        <#code#>
    //    }
    
    //    override func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
    //        return false
    //    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(120)
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("Scrolling end")
        if (refreshed) {
            refreshed = false
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == "storyDetailSegue",
            let destination = segue.destination as? BKStoryDetailViewController,
            let storyIndex = tableView.indexPathForSelectedRow?.row
        {
            destination.storyInfo = self.stories[storyIndex]
        }
    }
    
}

extension BKStoryListTableViewController: SMFeedbackDelegate {
    func respondentDidEndSurvey(_ respondent: SMRespondent!, error: Error!) {
        print("\(error)")
    }
    
    func displaySurvey() {
        surveyMonkeyFeedbackController?.present(from: self, animated: true, completion: { 
            print("Survey done")
        })
    }
}
