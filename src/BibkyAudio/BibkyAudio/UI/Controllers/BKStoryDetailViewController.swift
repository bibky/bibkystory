//
//  BKStoryDetailViewController.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 6/5/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

class BKStoryDetailViewController: UIViewController {
    
    var storyInfo: StoryInfo? = nil
    
    @IBOutlet weak var playingStoryView: UIView!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    var downloadStoryView: BKDownloadStoryView? = nil
    var startStoryView: BKStartStoryView? = nil
    
    @IBOutlet weak var storyLabel: UILabel!
    
    @IBOutlet weak var pauseButton: UIButton!
    
    @IBAction func onStoryPause(_ sender: Any) {
        NotificationCenter.default.post(name: bkPausePlayingNotification, object: nil)
    }
    
    @IBAction func onStartOver(_ sender: Any) {
        NotificationCenter.default.post(name: bkStartoverStoryNotification, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToForeground), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onStartedPlayingNotification), name: bkStartedPlayingNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onBibkyPauseStoryNotification), name: bkPausePlayingNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onBibkyPauseStoryNotification), name: bkStartoverStoryNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onBibkyPauseStoryNotification), name: bkFinishedPlayingNotification, object: nil)

        
        backgroundImageView.renderBackgroundImage(storyLocation: (storyInfo?.location)!, updateImage: !(storyInfo?.downloaded)!)
        updateUI()

    }
    
    func updateUI() {
        playingStoryView.isHidden = true
        storyLabel.isHidden = true
        
        // Do any additional setup after loading the view.
        if (!(storyInfo?.downloaded)!) {
            self.downloadStory()
        } else {
            if (BKStoryPlayerManager.shared.currentPlayingStoryMeta?.storyId == storyInfo?.id) {
                self.showPlayingStory()
            } else {
                self.showPlayStory()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func appMovedToForeground() {
        updateUI()
    }
    
    func downloadStory() {
        playingStoryView.isHidden = true
        self.downloadStoryView?.removeFromSuperview()
        self.startStoryView?.removeFromSuperview()
        
        downloadStoryView = Bundle.main.loadNibNamed("DownloadStoryView", owner: nil, options: nil)?.first as? BKDownloadStoryView
        self.downloadStoryView?.downloadProgressView.progress = 0

        self.view.addSubview(downloadStoryView!)
        downloadStoryView?.translatesAutoresizingMaskIntoConstraints = false
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[subview]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": downloadStoryView!]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[subview]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": downloadStoryView!]))

        
        BKFirebaseStorageManager.shared.downloadFromRemote(info: storyInfo!, progress: { (progress:Double?) in
            DispatchQueue.main.async{
                print("\(String(describing: progress))")
                self.downloadStoryView?.downloadProgressView.progress = Float(progress!)
            }
        }, complete: {() in
            // load show play view
            self.storyInfo?.downloaded = true
            BKLocalDbManager.shared.markStoryDownloaded(self.storyInfo!)
            self.showPlayStory()
        })
    }
    
    func showPlayStory() {
        playingStoryView.isHidden = true
        self.downloadStoryView?.removeFromSuperview()
        self.startStoryView?.removeFromSuperview()
        
        startStoryView = Bundle.main.loadNibNamed("StartStoryView", owner: nil, options: nil)?.first as? BKStartStoryView
        
        
        let transformAnim  = CAKeyframeAnimation(keyPath:"transform")
        let leftMove = NSValue(caTransform3D: CATransform3DMakeRotation(0.06, 0.0, 0.0, 1.0))
        let rightMove = NSValue(caTransform3D: CATransform3DMakeRotation(-0.06 , 0, 0, 1))
        transformAnim.values  = [leftMove,rightMove, leftMove, rightMove, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        transformAnim.autoreverses = true
        transformAnim.duration  = 1
        transformAnim.repeatCount = Float.infinity
        startStoryView?.phoneShakeImage.layer.add(transformAnim, forKey: "transform")
        
        self.view.addSubview(startStoryView!)

        startStoryView?.translatesAutoresizingMaskIntoConstraints = false
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[subview]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": startStoryView!]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[subview]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": startStoryView!]))
        
        BKLocalDbManager.shared.addToUpNext((storyInfo?.id)!)
    }
    
    func showPlayingStory() {
        playingStoryView.isHidden = false
        self.downloadStoryView?.removeFromSuperview()
        self.startStoryView?.removeFromSuperview()
    }
    
    func onStartedPlayingNotification(notification : NSNotification) {
        print("RECEIVED SPECIFIC NOTIFICATION: \(notification)")
        if (notification.object != nil) {
//            let storyId = notification.object as! Int
            DispatchQueue.main.async{
                self.showPlayingStory()
            }
        }
    }
    
    func onBibkyPauseStoryNotification(notification : NSNotification) {
        print("RECEIVED SPECIFIC NOTIFICATION: \(notification)")
        DispatchQueue.main.async {
            self.showPlayStory()
        }
    }

    override func didMove(toParentViewController parent: UIViewController?) {
        if (parent == nil) {
            NotificationCenter.default.removeObserver(self)
            NotificationCenter.default.post(name: bkPausePlayingNotification, object: nil)
            BKLocalDbManager.shared.resetUpNext()
        }
    }
    
}


@IBDesignable extension UIView {
    @IBInspectable var borderUIColor: UIColor {
        set {
            layer.borderColor = newValue.cgColor
        }
        
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
    }
    
}
