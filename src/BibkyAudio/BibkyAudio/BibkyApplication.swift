//
//  BibkyApplication.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 4/30/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

class BibkyApplication: UIApplication {
    var timer:Timer? = nil
    var duplicateDetectionCount:Int = 0
    var shakeCount:Int = 0
    var stopDetected:Bool = false
    
    override func sendEvent(_ event: UIEvent) {
        if event.subtype == UIEventSubtype.motionShake {
            print("Shake motion detected")
            NotificationCenter.default.post(name: shakeNotificationName, object: nil)
            
//            if (duplicateDetectionCount == 0) { //since we recieve 2 events here. Need to figure out why: Only on Simulator
////                duplicateDetectionCount += 1
//                if (!stopDetected) {
//                    NotificationCenter.default.post(name: shakeNotificationName, object: nil)
//                }
//                
//                
//                if (self.timer == nil) {
//                    DispatchQueue.main.async{
//                        self.timer = Timer.scheduledTimer(withTimeInterval: 8, repeats: false) { (timer) in
//                            self.timer = nil
//                            if(self.shakeCount >= 5) {
//                                // stop playing
//                                self.stopDetected = true
//                                NotificationCenter.default.post(name: bkPausePlayingNotification, object: nil)
//                                DispatchQueue.main.async{
//                                    Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { (timer) in
//                                        self.stopDetected = false
//                                    }
//                                }
//                            }
//                            self.shakeCount = 0
//                        }
//                    }
//                }
//            } else {
//                duplicateDetectionCount = 0
//            }
        }
        
        super.sendEvent(event)
    }
}
