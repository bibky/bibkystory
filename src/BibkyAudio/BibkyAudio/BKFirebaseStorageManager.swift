//
//  BKFirebaseManager.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 4/5/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit
import Firebase
import Zip

class BKFirebaseStorageManager: NSObject {
    
    var storageRef: StorageReference!
    
    // Can't init is singleton
    private override init() { }
    
    static let shared: BKFirebaseStorageManager = BKFirebaseStorageManager()
    
    func configure() {
        // Get a reference to the storage service using the default Firebase App
        let storage = Storage.storage()
        
        // Create a storage reference from our storage service
        storageRef = storage.reference()
    }
    
    func getImage(storyLocation: String, imageName: String, forceDownload:Bool, with block:@escaping (UIImage?) -> Void) {
        
        let filename = imageName.fileName()
        let fileExt = imageName.fileExtension()
        
        let scale = Int(UIScreen.main.scale)
        let _imageName = filename.appendingFormat("@%dx.", scale).appending(fileExt)
        
        // Create a reference to the file you want to download
        let paths = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        
        let destinationPath = documentsDirectory.appendingPathComponent(storyLocation)
        let destinationFileWithPath = destinationPath.appendingPathComponent(_imageName)
        
        if !forceDownload && FileManager.default.fileExists(atPath: destinationFileWithPath.path) {
            let image    = UIImage(contentsOfFile: destinationFileWithPath.path)
            block(image!)
        }
        
        do {
            try FileManager.default.createDirectory(atPath: destinationPath.path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription);
        }
        
        // [START downloadimage]
        let imageFileTreeRef = storageRef.child(storyLocation).child(_imageName)
        imageFileTreeRef.write(toFile: destinationFileWithPath, completion: { (url, error) in
            
            
            if let error = error {
                print("Error downloading:\(error)")
                block(nil)
            } else {
                
                if FileManager.default.fileExists(atPath: destinationFileWithPath.path) {
                    let image    = UIImage(contentsOfFile: destinationFileWithPath.path)
                    block(image!)
                }
            }
        })
        // [END downloadimage]
        
    }
    
    func downloadFromRemote(info: StoryInfo, progress progressBlock:@escaping (Double?) -> Void, complete completeBlock:@escaping (Void) -> Void) {
        // Create a reference to the file you want to download
        let islandRef = storageRef.child(info.location)
        
        let storyDataTreeRef = islandRef.child("data.zip")
        
        // Create local filesystem URL
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        let destinationPath = documentsDirectory.appendingPathComponent(info.location)
        do {
            try FileManager.default.createDirectory(atPath: destinationPath.path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription);
        }
        
        
        // [START downloadimage]
        DispatchQueue.global(qos: .background).async {
            let destinationFileWithPath = destinationPath.appendingPathComponent("data.zip")
            let downloadTask = storyDataTreeRef.write(toFile: destinationFileWithPath, completion: { (url, error) in
                if let error = error {
                    print("Error downloading:\(error)")
                } else {
                    // process json file from the server
                    do {
                        try Zip.unzipFile(destinationFileWithPath, destination: destinationPath, overwrite: true, password: nil, progress: { (progress) -> () in
                            print(progress)
                            if (progress >= 1) {
                                completeBlock()
                                NotificationCenter.default.post(name: bkDownloadCompleteNotification, object: nil)
                                do {
                                    try FileManager.default.removeItem(atPath: destinationFileWithPath.path)
                                } catch {
                                }
                            }
                        }) //
                    }
                    catch {
                        print("Something went wrong")
                    }
                }
            })
            // Create a task listener handle
            downloadTask.observe(.progress) { snapshot in
                // A progress event occurred
                if (snapshot.progress!.totalUnitCount > 0) {
                    let percentComplete = Double(snapshot.progress!.completedUnitCount)
                        / Double(snapshot.progress!.totalUnitCount)
                    progressBlock(percentComplete)
                }
            }
        }
        // [END downloadimage]
    }
    
    func downloadMedia(_ info: StoryInfo) {
        NotificationCenter.default.post(name: bibkyNotificationName, object: "Download started.")
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        
        var treeJson: [String] = readJson(info.location)
        treeJson.append("Thumbnail.png")
        
        
        let taskGroup = DispatchGroup()
        
        var success = true
        
        for path: String in treeJson {
            taskGroup.enter()
            
            // Create a reference to the file you want to download
            let islandRef = storageRef.child(info.location).child(path)
            
            let destinationPath = documentsDirectory.appendingPathComponent(info.location)
            let destinationFileWithPath = destinationPath.appendingPathComponent(path)
            do {
                try FileManager.default.createDirectory(atPath: destinationPath.path, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print(error.localizedDescription);
            }
            
            // [START downloadimage]
            islandRef.write(toFile: destinationFileWithPath, completion: { (url, error) in
                
                taskGroup.leave()
                
                if let error = error {
                    print("Error downloading:\(error)")
                    success = false
                } else {
                    // process json file from the server
                }
            })
            // [END downloadimage]
            
        }
        
        taskGroup.notify(queue: DispatchQueue.main, work: DispatchWorkItem(block: {
            if success {
                info.downloaded = true
                BKLocalDbManager.shared.updateDbStory(info)
            }
            NotificationCenter.default.post(name: bkDownloadCompleteNotification, object: info)
            NotificationCenter.default.post(name: bibkyNotificationName, object: "Download complete.")
            NotificationCenter.default.post(name: bibkyNotificationEvents, object: "reload")
        }))
        
    }
    
    private func readJson(_ location:String) -> [String] {
        do {
            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            let documentsDirectory = paths[0]
            
            let file = documentsDirectory.appendingPathComponent(location).appendingPathComponent("FileTree.json")
            let data = try Data(contentsOf: file)
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            if let object = json as? [String?] {
                // json is a array
                return object as! [String]
            } else {
                print("JSON is invalid")
            }
        } catch {
            print(error.localizedDescription)
        }
        return []
    }
    
    
}
