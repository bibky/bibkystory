//
//  BibkyAction.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 3/28/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

class BibkyAction: NSObject {

    fileprivate(set) var metadata: BibkyActionMeta
//    fileprivate(set) var delegate:BibkyActionDelegate!
    var actionCounter:Int = 0
    fileprivate(set) var completionHandler: ((BibkyActionMeta)->Void)!
    
    public init(_ metadata: BibkyActionMeta, completionHandler: @escaping ((BibkyActionMeta)->Void)) {
        self.metadata = metadata
        self.completionHandler = completionHandler
    }
    
//    public init(_ metadata: BibkyActionMeta, delegate: BibkyActionDelegate) {
//        self.metadata = metadata
//        self.delegate = delegate
//    }

    func start() {
        showActionInstruction()
        onFinishAction()
    }
    
    func stop() {
        
    }

    func onFinishAction() {
        self.stop()
        completionHandler(metadata)
//        delegate?.onActionFinish(action: self.metadata)
    }
    
    func showActionInstruction() {
        if (metadata.instruction != nil) {
            let instruction = String.init(format: "%@ [%d time(s)].", metadata.instruction!, metadata.actionCount)
            NotificationCenter.default.post(name: bibkyNotificationInstruction, object: instruction)
        }
    }

}
