//
//  JumpAction.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 3/28/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

let dataPoolSize: Int = 50
let filterSampleRate: Int = 5
let standardizationScale: Double = 2.0

class JumpAction: BibkyAction {
    fileprivate let motionManager = MotionManager(dataPoolSize: dataPoolSize)//, sampleingRate: filterSampleRate)
    var timer: Timer? = nil
    
//    public override init(_ metadata: BibkyActionMeta, delegate: BibkyActionDelegate) {
//        super.init(metadata, delegate: delegate)
//    }
    
    override func start() {
        self.timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(JumpAction.jumpDectection), userInfo: nil, repeats: true)
        motionManager.start()
        NotificationCenter.default.post(name: bibkyNotificationName, object: "Debug: Please Jump.")
        showActionInstruction()
    }
    
    override func stop() {
        timer?.invalidate()
        motionManager.stop()
    }
    
    func jumpDectection() {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            if self.motionManager.isJump() {
                self.onAction()
            }
        }
    }
    
    func onAction() {
        self.actionCounter += 1
        NotificationCenter.default.post(name: bibkyNotificationName, object: "Jump detected. Count: \(self.actionCounter)")
        if self.actionCounter >= metadata.actionCount {
            self.onFinishAction()
        }
    }
}


import CoreMotion

open class MotionManager {
    fileprivate let motionManager = CMMotionManager()
    fileprivate let motionActivityManager = CMMotionActivityManager()
    
    fileprivate let dataPoolSize: Int
    fileprivate var previousAccelerometerData: CMAccelerometerData?
    fileprivate(set) var datas = [CMAcceleration]()
    
    fileprivate let thresholdUp = -0.4
    fileprivate let thresholdDown = -1.4
    
    public init(dataPoolSize: Int) {
        self.dataPoolSize = dataPoolSize
    }
    
    public convenience init() {
        self.init(dataPoolSize: 50)
    }
    
    func start() {
        motionManager.startAccelerometerUpdates()
    }
    
    func stop() {
        motionManager.stopAccelerometerUpdates()
    }
    
    fileprivate func collectData() {
        if let data = motionManager.accelerometerData?.acceleration {
            
            if datas.count < dataPoolSize {
                datas.append(data)
            } else {
                datas.remove(at: 0)
                datas.append(data)
            }
        }
    }
    
    func isJump() -> Bool {
        var isJump = false
        collectData()
        
        if datas.count >= dataPoolSize {
            var _peaks = datas.map{ $0.x }
            if isJumpWithPeaks(_peaks) {
                isJump = true
            } else {
                _peaks = datas.map{ $0.y }
                if isJumpWithPeaks(_peaks) {
                    isJump = true
                } else {
                    _peaks = datas.map{ $0.z }
                    if isJumpWithPeaks(_peaks) {
                        isJump = true
                    }
                }
            }
            
            if isJump {
                datas.removeAll(keepingCapacity: false)
            }
        }
        return isJump
    }
    
    func isJumpWithPeaks(_ _peaks: [Double]) -> Bool {
        var isJump = false
        let peaks = peaksValuesFrom(_peaks)
        if peaks.count == 3 {
            if matchJumpPattern(peaks.map{ $0.0} ) { // match the down-up-down paatatern
                for peak in peaks {
                    let index = peak.1
                    if index < 10 || index > dataPoolSize-10 { // make sure the pattern is in the middle of the array to preven shaking or judging too early.
                        isJump = true
                        break
                    }
                }
                
            }
        }
        return isJump
    }
    
    fileprivate func matchJumpPattern(_ peaks: [Double]) -> Bool {
        if peaks[1] < peaks[0] || peaks[1] < peaks[2] {
            return false
        }
        
        if peaks[1] < thresholdUp {
            return false
        }
        
        if peaks[0] > thresholdDown && peaks[2] > thresholdDown {
            return false
        }
        
        return true
    }
    
    
    
    open func peaksValuesFrom(_ data: [Double]) -> [(Double, Int)] {
        var peaks = [(Double, Int)]()
        let samplingSizePerSide = 4
        let samplingSize = samplingSizePerSide * 2 + 1
        
        if data.count < samplingSize {
            return peaks
        }
        
        for index in 0...data.count-samplingSize {
            let sample = Array(data[index..<index+samplingSizePerSide*2])
            let middleIndex = index + samplingSizePerSide
            let middle = data[middleIndex]
            if isPeak(middle, values: sample) && (middle > thresholdUp || middle < thresholdDown) {
                peaks.append((middle, middleIndex))
            }
        }
        
        return peaks
    }
    
    open func isPeak(_ value: Double, values: [Double]) -> Bool {
        let min = values.reduce(value){ $0 < $1 ? $0 : $1 }
        let max = values.reduce(value){ $0 > $1 ? $0 : $1 }
        
        if value == min || value == max {
            return true
        }
        
        return false
    }
}

