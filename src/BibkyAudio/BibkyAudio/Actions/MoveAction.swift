//
//  MoveAction.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 3/29/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit
import CoreMotion

class MoveAction: BibkyAction {
    
    fileprivate let motionActivityManager = CMMotionActivityManager()
    var isInState: Bool = false
    var timer: Timer? = nil
    
//    public override init(_ metadata: BibkyActionMeta, delegate: BibkyActionDelegate) {
//        super.init(metadata, delegate: delegate)
//    }
    
    override func start() {
        startUpdatingActivity()
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(WalkAction.update), userInfo: nil, repeats: true);
        NotificationCenter.default.post(name: bibkyNotificationName, object: "Debug: Move action.")
        showActionInstruction()
    }
    
    override func stop() {
        if CMMotionActivityManager.isActivityAvailable() {
            self.motionActivityManager.stopActivityUpdates()
        }
        self.timer?.invalidate()
    }
    
    func startUpdatingActivity() {
        if CMMotionActivityManager.isActivityAvailable() {
            self.motionActivityManager.startActivityUpdates(to: OperationQueue.main, withHandler: {
                [weak self] (data: CMMotionActivity?) in
                DispatchQueue.main.async(execute: {
                    if let data = data {
                        if !data.stationary {
                            self?.isInState = true
                        }
                        
                        NSLog("stationary: \(data.stationary)")
                        NSLog("walking: \(data.walking)")
                        NSLog("running: \(data.running)")
                        NSLog("automotive: \(data.automotive)")
                        NSLog("unknown: \(data.unknown)")
                        NSLog("confidence(0-2): \(data.confidence.rawValue)")
                    }
                })
            })
        }
    }
    
    // must be internal or public.
    func update() {
        if (self.isInState) {
            self.actionCounter += 1
            NotificationCenter.default.post(name: bibkyNotificationName, object: "Movement detected. Count: \(self.actionCounter)")
            if self.actionCounter >= metadata.actionCount {
                self.onFinishAction()
            }
        }
    }
}
