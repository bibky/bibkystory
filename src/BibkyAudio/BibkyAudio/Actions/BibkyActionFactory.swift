//
//  BibkyActionFactory.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 3/28/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

protocol BibkyActionDelegate{
    func onActionFinish(action:BibkyActionMeta)
}


class BibkyActionFactory: NSObject {

//    class func createAction(_ actionMeta: BibkyActionMeta, delegate: BibkyActionDelegate) -> BibkyAction? {
//        // based on the action type, we create respective action object
//        var action: BibkyAction? = nil
//        switch actionMeta.type {
//        case BibkyActionType.NoAction:
//            action = BibkyAction(actionMeta, completionHandler: delegate)
//            break
//        case BibkyActionType.ShakeAction:
//            action = ShakeAction(actionMeta, delegate: delegate)
//            break
//        case BibkyActionType.JumpAction:
//            action = JumpAction(actionMeta, delegate: delegate)
//            break
//        case BibkyActionType.WalkAction:
//            action = WalkAction(actionMeta, delegate: delegate)
//            break
//        case BibkyActionType.RunAction:
//            action = RunAction(actionMeta, delegate: delegate)
//            break
//        case BibkyActionType.StepAction:
//            action = StepCountAction(actionMeta, delegate: delegate)
//            break
//        default: break
//        }
//        return action
//    }

    class func createAction(_ actionMeta: BibkyActionMeta, completionHandler: @escaping ((BibkyActionMeta)->Void)) -> BibkyAction? {
        // based on the action type, we create respective action object
        var action: BibkyAction? = nil
        
#if (arch(i386) || arch(x86_64)) && os(iOS)
        action = ShakeAction(actionMeta, completionHandler: completionHandler)
#else
        switch actionMeta.type {
        case BibkyActionType.NoAction:
            action = BibkyAction(actionMeta, completionHandler: completionHandler)
            break
        case BibkyActionType.ShakeAction:
            action = ShakeAction(actionMeta, completionHandler: completionHandler)
            break
        case BibkyActionType.JumpAction:
            action = JumpAction(actionMeta, completionHandler: completionHandler)
            break
        case BibkyActionType.WalkAction:
            action = WalkAction(actionMeta, completionHandler: completionHandler)
            break
        case BibkyActionType.RunAction:
            action = RunAction(actionMeta, completionHandler: completionHandler)
            break
        case BibkyActionType.StepAction:
            action = StepCountAction(actionMeta, completionHandler: completionHandler)
            break
        case BibkyActionType.MoveAction:
            action = MoveAction(actionMeta, completionHandler: completionHandler)
            break
        case BibkyActionType.TiltAction:
            action = TiltAction(actionMeta, completionHandler: completionHandler)
            break
    }
#endif
        return action
    }
}
