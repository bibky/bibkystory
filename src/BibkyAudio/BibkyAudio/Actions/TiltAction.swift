//
//  TiltAction.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 6/7/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit
import CoreMotion

class TiltAction: BibkyAction {

    fileprivate let motionManager = CMMotionManager()
    let queue = OperationQueue()

    override func start() {
        NotificationCenter.default.post(name: bibkyNotificationName, object: "Debug: Please Tilt your device.")
        if motionManager.isDeviceMotionAvailable {
            motionManager.deviceMotionUpdateInterval = 0.1
            motionManager.startDeviceMotionUpdates(to: .main) {
                [weak self] (data: CMDeviceMotion?, error: Error?) in
                if let gravity = data?.gravity {
                    let angle = atan2(gravity.x, gravity.y) + .pi/2;           // in radians
                    let angleDegrees = angle * 180.0 / .pi;   // in degrees
                    print ("Rotation: \(angleDegrees)")
//                    self.onAction()
                }
            }
        }
        showActionInstruction()
    }
    
    override func stop() {
        motionManager.stopDeviceMotionUpdates()
    }
    
    func onAction() {
        self.actionCounter += 1
        NotificationCenter.default.post(name: bibkyNotificationName, object: "Jump detected. Count: \(self.actionCounter)")
        if self.actionCounter >= metadata.actionCount {
            self.onFinishAction()
        }
    }

}
