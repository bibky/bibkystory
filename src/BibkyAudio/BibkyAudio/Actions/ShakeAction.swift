//
//  ShakeAction.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 3/28/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

class ShakeAction: BibkyAction {
    
//    public override init(_ metadata: BibkyActionMeta, delegate: BibkyActionDelegate) {
//        super.init(metadata, delegate: delegate)
//    }
    
    override func start() {
        NotificationCenter.default.addObserver(self, selector: #selector(ShakeAction.onShakeNotification), name: shakeNotificationName, object: nil)
        NotificationCenter.default.post(name: bibkyNotificationName, object: "Debug: Shake the device.")
        showActionInstruction()
    }
    
    override func stop() {
        NotificationCenter.default.removeObserver(self)
    }


    func onShakeNotification(notification : NSNotification) {
        print("RECEIVED SPECIFIC NOTIFICATION: \(notification)")
        self.actionCounter += 1
        NotificationCenter.default.post(name: bibkyNotificationName, object: "Debug: Shake detected. Count: \(self.actionCounter)")
        if self.actionCounter >= metadata.actionCount {
            self.onFinishAction()
        }
    }
}
