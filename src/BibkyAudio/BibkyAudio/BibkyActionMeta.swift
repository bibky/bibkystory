//
//  BibkyActionMeta.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 3/28/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

public enum BibkyActionType : Int {
    case NoAction // count = 0
    case ShakeAction // count = 1
    case JumpAction // count = 1
    case StepAction // count = 10 steps
    case WalkAction // count = 5 (secs)
    case RunAction // count = 5 (secs)
    case MoveAction // count = 5 (secs)
    case TiltAction
}

func convertActionType(type: Int) -> BibkyActionType {
    var _type = BibkyActionType.NoAction
    switch type {
    case 1:
        _type = BibkyActionType.ShakeAction
        break
    case 2:
        _type = BibkyActionType.JumpAction
        break
    case 3:
        _type = BibkyActionType.StepAction
        break
    case 4:
        _type = BibkyActionType.WalkAction
        break
    case 5:
        _type = BibkyActionType.RunAction
        break
    case 6:
        _type = BibkyActionType.MoveAction
        break
    case 7:
        _type = BibkyActionType.TiltAction
        break
    default:
        _type = BibkyActionType.NoAction
    }
    return _type
}

class BibkyActionMeta: NSObject {
    
    fileprivate(set) var type: BibkyActionType
    var actionAudio: BibkyAudioMeta? = nil
    var actionEndAudio: BibkyAudioMeta? = nil
    var nextChapterId: Int = 0
    var actionCount: Int = 0
    var instruction: String? = nil
    
    // version2
    var startTime: Double = 0.0
    var endTime: Double = 0.0
    var repeatStartTime: Double = 0.0
    var endActionJumpTime: Double = 0.0
    var repeatDelay: Double = 30 // seconds
    var disableEndActionJump: Bool = false
    

    public init(_ type: BibkyActionType) {
        self.type = type
        
    }
    
}
