//
//  main.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 4/30/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import UIKit

let argc = CommandLine.argc
let argv = UnsafeMutableRawPointer(CommandLine.unsafeArgv).bindMemory(to: UnsafeMutablePointer<Int8>.self, capacity: Int(CommandLine.argc))

UIApplicationMain(argc, argv, NSStringFromClass(BibkyApplication.self), NSStringFromClass(AppDelegate.self))
