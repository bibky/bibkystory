//
//  BKDataManager.swift
//  BibkyAudio
//
//  Created by Anirban Maiti on 4/4/17.
//  Copyright © 2017 Bibky. All rights reserved.
//

import Foundation
import Firebase

class BKDataManager: NSObject {
    
    // Can't init is singleton
    private override init() { }
    
    static let shared: BKDataManager = BKDataManager()
    
    var bibkyStories:[StoryInfo] = []
    var bibkyStoryMetadata:[BibkyStoryMeta] = []
    
    func configure() {
        
        
        // [START firebase_configure]
        // Use Firebase library to configure APIs
//        var googleServiceFile = "GoogleService-Info"
//        #if APP_STORE
//            googleServiceFile = "GoogleServiceProd-Info"
//        #endif
//        let filePath = Bundle.main.path(forResource: googleServiceFile, ofType: "plist")!
//        let options = FIROptions(contentsOfFile: filePath)
        FirebaseApp.configure()
        // [END firebase_configure]
        
        // [START storageauth]
        // Using Cloud Storage for Firebase requires the user be authenticated. Here we are using
        // anonymous authentication.
        if Auth.auth().currentUser == nil {
            Auth.auth().signInAnonymously(completion: { (user: User?, error: Error?) in
                if let error = error {
                    print(error)
                } else {
                }
            })
        }
        // [END storageauth]
        // Configure tracker from GoogleService-Info.plist.
//        var configureError: NSError?
//        GGLContext.sharedInstance().configureWithError(&configureError)
//        assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
        
        // Optional: configure GAI options.
        guard let gai = GAI.sharedInstance() else {
            assert(false, "Google Analytics not configured correctly")
            return
        }
//        GAI.sharedInstance().tracker(withTrackingId: "UA-XXXX-YY")
        gai.tracker(withTrackingId: "UA-101175270-1")
        gai.trackUncaughtExceptions = true  // report uncaught exceptions
        gai.logger.logLevel = .verbose

        
        BKFirebaseStorageManager.shared.configure()
        BKFirebaseDbManager.shared.configure()
        BKLocalDbManager.shared.configure()
        BKStoryPlayerManager.shared.configure()
        
        // Onfirst launch - move the preloaded files to Documents directory
//        #if (arch(i386) || arch(x86_64)) && os(iOS)
//            copyData()
//        #else
//            
//            let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
//            if !launchedBefore  {
//                print("First launch, setting UserDefault.")
//                UserDefaults.standard.set(true, forKey: "launchedBefore")
//                
//                // query server to get the data
////                copyData()
//            } else {
//                print("Not first launch.")
//            }
//        #endif
        
//        BKFirebaseDbManager.shared.subscribe()

        NotificationCenter.default.addObserver(self, selector: #selector(self.onDownloadCompleteNotification), name: bkDownloadCompleteNotification, object: nil)
    }
    
    
    func onDownloadCompleteNotification(notification : NSNotification) {
        print("RECEIVED SPECIFIC NOTIFICATION: \(notification)")
        DispatchQueue.main.async {
//            self.invalidateStoryList()
            self.invalidateStoryMetadata()
        }
    }
    
}

// extension for Bibky Stories
extension BKDataManager {
    
    func invalidateStoryList() {
        BKDataCache.shared.clearStories()
    }
    
    func storyInfoList(_ reload:Bool = false) -> [StoryInfo] {
        
        // 1. get from the cache
        // 2. If cache empty, get from the local database
        // 3. If thats empty, query server to get the data
        
        var stories:[StoryInfo] = []
        stories = BKDataCache.shared.getStories()
        if (stories.count > 0 && !reload) {
            return stories
        }
        
        stories = []
        if let dbStories:[Story] = BKLocalDbManager.shared.fetchStories() {
            for story in dbStories {
                let s = StoryInfo()
                if let id = Int(story.id) as? Int,
                    let location = story.location! as? String,
                    let name = story.name! as? String,
                    let description = story.story_desc! as? String,
                    let length = story.length! as? String,
                    let authors = story.authors! as? String,
                    //                    let bgColor = story.bgColor! as? String,
                    let version = story.version as? Double,
                    let downloaded = story.downloadState as? Bool
                {
                    s.id = id
                    s.location = location
                    s.name = name
                    s.version = version
                    s.story_desc = description
                    s.length = length
                    s.authors = authors
                    s.downloaded = downloaded
                    
                    stories.append(s)
                }
            }
        }
        if (stories.count > 0) {
            return stories
        }

//        BKFirebaseDbManager.shared.getStories { (storyInfos) in
//        }
        return stories
    }
    
    func getStoryInfo(byId id:Int) -> StoryInfo? {
        let stories = storyInfoList()
        for _story in stories {
            if _story.id == id {
                return _story
            }
        }
        return nil
    }
    
}

// extension for Bibky Story Metadata
extension BKDataManager {
    
    func invalidateStoryMetadata() {
        BKDataCache.shared.clearMetadata()
    }

    func getStoryMetadata(_ info: StoryInfo) -> BibkyStoryMeta? {

        let metadataList:[BibkyStoryMeta] = BKDataCache.shared.getMetadata()!
        for _storyMeta in metadataList {
            if _storyMeta.storyId == info.id {
                return _storyMeta
            }
        }
        
        var storyMeta:BibkyStoryMeta? = nil
        if let json:[String: Any] = readDocumentDirectoryJson(fileName: "storyMetadata", subdirectory: info.location) {
            info.playerVersion = BibkyPlayerVersion.Ver3
            storyMeta = buildStoryMetadata(info: info, jsonData: json)
        }
        if (storyMeta != nil) {
            BKDataCache.shared.addMetadata(storyMeta!)
        }
        return storyMeta
    }
    
    func getStoryMetadata(byId id:Int) -> BibkyStoryMeta? {
        if let storyInfo = getStoryInfo(byId: id) {
            return getStoryMetadata(storyInfo)
        }
        return nil
    }
    
    private func buildStoryMetadata(info: StoryInfo, jsonData: [String: Any] ) -> BibkyStoryMeta{
        
        let story = BibkyStoryMeta(info)
        story.storyDescription = info.description
        story.storyAuthors = info.authors
        story.storyLength = info.length
        
        
        let chapters = (jsonData["chapters"] as? [[String: Any]])!
        
        for chapter in chapters {
            
            if let id = chapter["id"] as? Int,
                let action_containers = chapter["action-containers"] as? [[String: Any]] {
                
                let chapterMeta = story.addChapter(id)
                
                if let audio = chapter["backgroundAudio"] as? [String: Any],
                    let bkAudio = audio["url"] as? String {
                    
                    var loop = false
                    if let _loop = audio["loop"] as? Bool {
                        loop = _loop
                    }
                    
                    let audioPath = info.location.appendingFormat("/%@", bkAudio)
                    let bgMedia = BibkyAudioMeta(audioPath, loop: loop)
                    if let playTimer = audio["playTimer"] as? Double {
                        bgMedia.playTimer = playTimer
                    }
                    chapterMeta.backgroundAudio = bgMedia
                }
                if let audio = chapter["narrationAudio"] as? [String: Any],
                    let bkAudio = audio["url"] as? String{
                    
                    let audioPath = info.location.appendingFormat("/%@", bkAudio)
                    let narMedia = BibkyAudioMeta(audioPath, loop: false)
                    chapterMeta.setNarration(narMedia)
                }
                
                for container in action_containers {
                    if let containerId = container["id"] as? Int,
                        let startTime = container["startTime"] as? String,
                        let endTime = container["endTime"] as? String,
                        let actions = container["actions"] as? [[String: Any]]{
                        
                        let containerMetadata = chapterMeta.addActionContainer(id: containerId)
                        containerMetadata.startTime = convertStringTime(startTime)
                        containerMetadata.endTime = convertStringTime(endTime)
                        
                        if let instruction = container["instruction"] as? String {
                            containerMetadata.instruction = instruction
                        }
                        
                        if let repeatStartTime = container["repeatStartTime"] as? String {
                            containerMetadata.repeatStartTime = convertStringTime(repeatStartTime)
                        }
                        if let endActionJumpTime = container["endActionJumpTime"] as? String {
                            containerMetadata.endActionJumpTime = convertStringTime(endActionJumpTime)
                        }
                        
                        if let disableEndActionJump = container["disableEndActionJump"] as? Bool {
                            containerMetadata.disableEndActionJump = disableEndActionJump
                        }
                        
                        if let repeatDelay = container["repeatDelay"] as? Double {
                            containerMetadata.repeatDelay = repeatDelay
                        }
                        
                        for action in actions {
                            if let actionType = action["type"] as? Int,
                                let actionCount = action["actionCount"] as? Int {
                                
                                let actionMetadata = containerMetadata.addAction(convertActionType(type: actionType))
                                actionMetadata.actionCount = actionCount
                                if let nextChapterId = action["nextChapterId"] as? Int {
                                    actionMetadata.nextChapterId = nextChapterId
                                }
                            }
                        }
                    }
                    
                }
            }
        }
        
        return story
    }
    
    private func readDocumentDirectoryJson(fileName: String, subdirectory: String) -> [String: Any]? {
        do {
            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            let documentsDirectory = paths[0]
            let file = documentsDirectory.appendingPathComponent(subdirectory).appendingPathComponent("\(fileName).json")
            
            if FileManager.default.fileExists(atPath: file.path) {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    return object
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return [:]
    }


}

// extension for Bibky Images
extension BKDataManager {
    func getThumbnailImage(storyLocation: String, forceDownload:Bool, with block:@escaping (UIImage?) -> Void) {
        let key = "\(storyLocation).Thumbnail"
        if let image:UIImage = BKDataCache.shared.getImage(key: key) {
            block(image)
        } else {
            BKFirebaseStorageManager.shared.getImage(storyLocation: storyLocation, imageName: "Thumbnail.png", forceDownload: forceDownload) { (image) in
                block(image)
                BKDataCache.shared.addImage(key: key, image: image!)
            }
        }
    }
    
    func getBackgroundImage(storyLocation: String, forceDownload:Bool, with block:@escaping (UIImage?) -> Void) {
        let key = "\(storyLocation).Background"
        if let image:UIImage = BKDataCache.shared.getImage(key: key) {
            block(image)
        } else {
            BKFirebaseStorageManager.shared.getImage(storyLocation: storyLocation, imageName: "Background.png",forceDownload: forceDownload) { (image) in
                block(image)
                BKDataCache.shared.addImage(key: key, image: image!)
            }
        }
    }
}

// extension for Data Preloader
extension BKDataManager {
    private func copyData() {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        
        let treeJson: [String] = readJson()
        var success = false
        
        for path: String in treeJson {
            var words = path.components(separatedBy: "/")
            let fileNameWithExt = words.removeLast()
            let fileNameWords = fileNameWithExt.components(separatedBy: ".")
            let fileName = fileNameWords.first!
            let fileExt = fileNameWords.last!
            let directory = words.joined(separator: "/")
            
            do {
                let sourcePath: URL? = Bundle.main.url(forResource: fileName, withExtension: fileExt, subdirectory: directory)
                if (sourcePath != nil) && !FileManager.default.fileExists(atPath: (sourcePath?.path)!) {
                    NSLog("The source file is not be found.")
                    return
                }
                
                
                let destinationPath = documentsDirectory.appendingPathComponent(directory)
                let destinationFileWithPath = documentsDirectory.appendingPathComponent(path)
                do {
                    try FileManager.default.createDirectory(atPath: destinationPath.path, withIntermediateDirectories: true, attributes: nil)
                } catch let error as NSError {
                    print(error.localizedDescription);
                }
                
                do {
                    try FileManager.default.removeItem(atPath: destinationFileWithPath.path)
                } catch {
                }
                
                try FileManager.default.copyItem(atPath: (sourcePath?.path)!, toPath: destinationFileWithPath.path)
                success = true
                
            } catch let error as NSError {
                // Catch fires here, with an NSError being thrown
                print("error occurred, here are the details:\n \(error)")
            }
        }
        
        if (success) {
            BKLocalDbManager.shared.preloadData()
        }
        
    }
    
    // Intial StoryTree - Do not change this main.url
    private func readJson() -> [String] {
        do {
            if let file = Bundle.main.url(forResource: "StoryTree", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String?] {
                    // json is a array
                    return object as! [String]
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return []
    }

}
